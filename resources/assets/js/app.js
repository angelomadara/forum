
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

/** bootstrap css */
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

/** vue select */
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

/** vue textbox with controls */
import VueSimplemde from 'vue-simplemde'
Vue.use(VueSimplemde)
Vue.component('vue-simplemde', VueSimplemde)

import md from 'marked'
window.md = md

/** sweet alert */
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2)

/** font awesome */
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// library.add(FontAwesomeIcon)

import User from './helpers/User'
window.User = User

window.EventBus = new Vue();

// console.log(User.loggedIn())

/** filters */
Vue.filter('shorten',function(str){
    let maxLength = 300;
    if(str.length >= 300){
        var trimmedString = str.substr(0, maxLength);
        return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))+'...';
    }
    return str;
});

Vue.filter('plural',function(noun,sufix,count){
    if(count > 1) return noun+sufix;
    else return noun;
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('home', require('./components/Home.vue'));

/**
 * vue router
 * this will be the frontend application routes
 * that will communicate the backend API routes (routes/api.php)
 */
import router from './router/router.js'

const app = new Vue({
    el: '#app',
    router,
});
