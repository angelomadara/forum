import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Login from '../components/auth/Login'
import Signup from '../components/auth/Signup'
import Logout from '../components/auth/Logout'

/**
 * public pages
 */
import Index from '../components/pages/Index'
import pageNotFound from '../components/pages/PageNotFound'
import SignupComplete from '../components/pages/SignupComplete'
import Ask from '../components/pages/Ask'
import ViewQuestion from '../components/pages/ViewQuestion'
import Profile from '../components/pages/Profile'
import SettingsProfile from '../components/pages/SettingsProfile'
import Someone from '../components/pages/Someone'
import FindMentor from '../components/pages/FindMentor'
import Students from '../components/pages/Students'
import Mentors from '../components/pages/Mentors'
import Conversations from '../components/pages/Conversation'
import ConfirmEmail from '../components/pages/ConfirmEmail'

/**
 * admin pages
 */
import AdminPage from '../components/admin/AdminPage'
import AdminAccountList from '../components/admin/AccountMasterlist'
import AdminAccountReports from '../components/admin/ReportedAccounts'
import ManageTags from '../components/admin/ManageTags'


/**
 * public routes
 */
const routes = [
    {path: '/404',component: pageNotFound,name:'404'},

    /** common routes */
    {path: '/login',component: Login},
    {path: '/logout',component: Logout},
    {path: '/signup',component: Signup},
    {path: '/signup-complete',component: SignupComplete, name: 'signup_complete'},
    {path: '/',component: Index, name: 'index'},
    {path: '/home',component: Index, name: 'index'},
    {path: '/profile',component: Profile, name: 'profile'},
    {path: '/settings/profile',component: SettingsProfile, name: 'settings_profile'},
    {path: '/profile/:user_code',component: Someone, name: 'someone'},
    {path: '/ask',component: Ask, name: 'ask'},
    {path: '/my-mentors',component: Mentors, name: 'my_mentors'},
    {path: '/find-mentor',component: FindMentor, name: 'find_mentor'},
    {path: '/my-students',component: Students, name: 'students'},
    {path: '/question/:id/:slug',component: ViewQuestion, name: 'view_question'},
    {path: '/conversation/:student_id',component: Conversations, name: 'conversations'},
    {path: '/email/confirm/:code',component: ConfirmEmail, name: 'email_confirm'},


    /** admin */
    {path: '/admin',component: AdminPage, name: 'admin'},
    {path: '/admin/accounts-list',component: AdminAccountList},
    {path: '/admin/accounts-report',component: AdminAccountReports},
    {path: '/admin/manage-tags',component: ManageTags},
]

/**
 * initialize the public routes above
 */
const router = new VueRouter({
    routes : routes,
    hashbang : false, // removes hash in url
    mode : 'history' // helps remove hash in url
})

export default router
