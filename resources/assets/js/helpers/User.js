import Token from "./Token"
import AppStorage from "./AppStorage";

class User{
    // login(data){
    //     axios.post('/api/auth/login',data)
    //     .then(res => this.responseAfterLogin(res))
    //     .catch(err => {
    //         return err.response;
    //     })
    // }

    responseAfterLogin(res){
        const _token = res.data.access_token;
        const _user = JSON.stringify(res.data.user);

        if(Token.isValid(_token)){
            AppStorage.store(_user,_token)
            window.location = '/home'
        }
    }

    hasToken(){
        const storedToken = AppStorage.getToken();
        if(storedToken){
            return Token.isValid(storedToken);
        }
        return false
    }

    loggedIn(){
        return this.hasToken();
    }

    logout(){
        AppStorage.clear()
        window.location = '/home'
    }

    profile(){
        if(this.loggedIn){
            return AppStorage.getUser()
        }
        return false
    }

    id(){
        if(this.loggedIn){
            return AppStorage.getUser().id
        }
        return false
    }

    isOwn(id){
        return this.id() == id 
    }

    isAdministrator(){
        if(this.loggedIn()){
            return this.id() == 1 && this.loggedIn()
        }
        return false
    }

    roleName(){
        if(this.loggedIn){
            return AppStorage.getUser().roles[0].name
        }
        return false
    }

    roleId(){
        if(this.loggedIn){
            return AppStorage.getUser().roles[0].id
        }
        return false
    }


    isModerator(){

    }
}

export default User = new User();