<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            [
                'name' => "administrator",
                'display_name' => "Administrator",
            ],[
                'name' => "moderator",
                'display_name' => "Moderator",
            ],[
                'name' => "mentor",
                'display_name' => "Mentor",
            ],[
                'name' => "student",
                'display_name' => "Student",
            ]
        ]);
    }
}
