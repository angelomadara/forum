<?php

use Illuminate\Database\Seeder;

class InitialUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \DB::table('users')->insert([
            'name' => "Administrator",
            'email' => "admin@bpsuforum.com",
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'remember_token' => str_random(10),
            'student_no' => '00-0000',
            'active' => '1',
        ]);
    }
}
