<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewSettingsForUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('photo_lg')->after('email')->nullable();
            $table->string('photo_sm')->after('photo_lg')->nullable();
            $table->string('mood')->after('active')->nullable();
            $table->string('skype')->after('mood')->nullable();
            $table->string('linkedin')->after('skype')->nullable();
            $table->string('twitter')->after('linkedin')->nullable();
            $table->string('facebook')->after('twitter')->nullable();
            $table->string('website')->after('facebook')->nullable();
            $table->string('organization')->after('website')->nullable();
            $table->text('bio')->after('organization')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'photo_lg',
                'photo_sm',
                'mood',
                'skype',
                'linkedin',
                'twitter',
                'facebook',
                'website',
                'organization',
                'bio',
            ]);
        });
    }
}
