<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StreamingProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('discord')->nullable()->after('active');
            $table->string('facebook_live')->nullable()->after('discord');
            $table->string('twitch')->nullable()->after('facebook_live');
            $table->string('mixer')->nullable()->after('twitch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['discord','facebook_live','twitch','mixer']);
        });
    }
}
