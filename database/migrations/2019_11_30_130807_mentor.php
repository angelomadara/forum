<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mentor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follow_mentor', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id')->comment('users id');
            $table->unsignedInteger('mentor_id')->comment('users id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_id')->on('users')->references('id');
            $table->foreign('mentor_id')->on('users')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('follow_mentor');
    }
}
