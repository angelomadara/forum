<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Reply;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function like(Reply $reply){
        $reply->like()->create(['user_id'=>1]);
    }

    public function removeLike(Reply $reply){
        $reply->like()->where(['user_id'=>1])->first()->delete();
    }
}
