<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Mail\ConfirmEmail;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login']]);
        $this->middleware('jwt', ['except' => ['login','signup','confirm_email']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {

        // $credentials = request(['email', 'password']);
        $credentials = request(['student_no', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
        // return $this->respondWithToken($this->me());
    }

    public function signup(UserRequest $get){
        // ['name', 'student_no','email','password','role']
        $get['email_token'] = encrypt($get->student_no); // generate email token

        $user = User::create($get->all()); // register
        $user->roles()->attach($get->role); // attach role

        Mail::to($get->email)->send(new ConfirmEmail($get->email_token)); // send email

        return $this->login($get->all());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user();
        $user->roles;
        return response()->json($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user = auth()->user();
        $user->roles;
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $user,
        ]);
    }

    public function payload(){
        return auth()->payload();
    }

    public function returnRole(){
        return auth()->user()->roles;
    }

    // public function confirm_email($_token){
    //     // return $_token;
    //     // User::where('email_token',$_token)->update(['confirmed'=>1]);
    //     return redirect('login')->with("success","Email successfully confirmed");
    // }

    public function emailCheckCode($email_token){
        $user = User::where('email_token',$email_token);
        $found_user = $user->first();
        if($found_user){

            if($found_user->confirmed == 0){
                $user->update(['confirmed'=>1]);
                return [
                    'response' => 'Email successully conrimed',
                    'status' => 'true'
                ];
            }

            return [
                'response' => 'Email is already been confirmed',
                'status' => 'true'
            ];
        }

        return [
            'response' => 'Incorrect email verification link',
            'status' => 'false'
        ];
    }

    public function someone($student_no){
        $user = User::where('student_no',$student_no)->first();
        $user->roles;
        return response()->json($user);
    }

    public function role(){
        return auth()->user()->roles;
    }
}
