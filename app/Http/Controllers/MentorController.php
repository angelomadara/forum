<?php

namespace App\Http\Controllers;

use App\Models\FollowMentor;
use App\User;
use Illuminate\Http\Request;

class MentorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * 1 - administrator
         * 2 - moderator
         * 3 - mentor
         * 4 - student
         */
        $role_number = 3;
        $me = auth()->id();
        $mentors = \DB::select("SELECT
            u.id, u.name, u.student_no, u.photo_lg, u.email, u.discord, u.facebook_live, u.twitch, u.mixer,
            u.skill_summary, u.bio, ru.role_id
            FROM users as u LEFT JOIN role_user as ru ON u.id = ru.user_id
            WHERE ru.role_id = ? AND u.id != ?
        ",[$role_number,$me]);
        return response()->json($mentors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth()->user()->id;
        $request->request->add(['student_id'=>$user_id]);
        // return $request->all();

        $a = FollowMentor::where('student_id',$user_id)->where('mentor_id',$request->mentor_id)->first();
        $mentor = User::where('id',$request->mentor_id)->first();

        if(!$a){
            FollowMentor::create($request->all());
            return [
                'status'=> 'success',
                'message'=> 'You are now following'.$mentor->name,
            ];
        }

        return [
            'status' => 'warning',
            'message'=> 'You already following '.$mentor->name,
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mentors = \DB::select("SELECT
            u.id, u.name, u.student_no, u.photo_lg, u.email, u.discord, u.facebook_live, u.twitch, u.mixer,
            u.skill_summary, u.bio
            FROM follow_mentor as fm LEFT JOIN users as u ON fm.mentor_id = u.id
            WHERE fm.student_id = ? AND fm.deleted_at IS NULL
        ",[$id]);
        return response()->json($mentors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unfollow(Request $get){
        $id = auth()->user()->id;
        $mentor_id = $get->mentor_id;
        $a = FollowMentor::where('student_id',$id)->where('mentor_id',$mentor_id)->delete();
        $mentor = User::where('id',$mentor_id)->first();
        if($a){
            return [
                'status'=> 'success',
                'message'=> 'You are successfully unfollow '.$mentor->name,
            ];
        }
        return [
            'status' => 'warning',
            'message'=> 'Something went wrong',
        ];
    }

    public function followed(){
        $id = auth()->user()->id;
        $mentors = FollowMentor::where('student_id',$id)->get();
        $a=[];
        foreach ($mentors as $key => $value) {
            $a[$key] = $value->mentor_id;
        }
        return $a;
    }
}
