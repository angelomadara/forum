<?php

namespace App\Http\Controllers;

use App\User;
use Image;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return \DB::table('users as u')->select('u.*')->leftJoin('role_user as ru','u.id','=','ru.id')->get();
        return auth()->user();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::where('id',$id)->update($request->all());
        return [
            'status' => true,
            'message' => "success"
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeAvatar(Request $get){

        $id = auth()->user()->id;

        $file = $get->photo;

        $file_ext_name 	= strtolower($file->getClientOriginalExtension());
        $rand_name = str_random().'.'.$file_ext_name;
        $new_file_name = 'img/avatar/'.$rand_name;
        $new_file_name_small = 'img/avatar/small/'.$rand_name;

        Image::make($get->photo)->resize(400, null,function($constraint){
            $constraint->aspectRatio();
            $constraint->upsize();
        // })->save(public_path($new_file_name ));
        })->save($new_file_name );

        Image::make($get->photo)->resize(100, null,function($constraint){
            $constraint->aspectRatio();
            $constraint->upsize();
        // })->save(public_path($new_file_name_small ));
        })->save($new_file_name_small );

        User::where('id',$id)->update([
            'photo_lg' => '/'.$new_file_name,
            'photo_sm' => '/'.$new_file_name_small
        ]);

        return [
            'status' => 'success',
            'message' => "Avatar changed",
            'big' => '/'.$new_file_name,
        ];
    }
}
