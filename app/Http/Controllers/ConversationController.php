<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Models\Conversation;
use App\Models\ConversationUsers;
use App\User;
use Illuminate\Http\Request;

class ConversationController extends Controller
{
    public function index($id){
        $convo_id = $this->chat($id);
        $chats = Conversation::where('conversation_user_id',$convo_id['convo_id'])->get();
        return [
            'convo_id' => $convo_id['convo_id'],
            'chats' => $chats,
        ];
    }

    public function chat($id){
        $from = auth()->id();
        $to = $id;
        $conversation_id1 = $from.$to;
        $conversation_id2 = $to.$from;
        $participants = ConversationUsers::where('participants',$conversation_id1)->orWhere('participants',$conversation_id2)->first();
        if(!$participants){
            $participants = ConversationUsers::create(['participants'=>$conversation_id1]);
        }

        return [
            'convo_id'=>$participants->id
        ];
    }

    public function send(Request $get){
        // return $get->all();
        $user = auth()->user();
        $user = ['from_id'=>$user->id, 'from_name'=>$user->name];
        $get->request->add($user);
        Conversation::create($get->all());
        event(new NewMessage($get->conversation_user_id));
        return [
            'status' => 200,
        ];
    }

    public function create($from,$to,$body){
        return Conversation::create([
            'from' => $from,
            'to' => $to,
            'body' => $body
        ]);
    }

    public function checkParticipants(){

    }

}
