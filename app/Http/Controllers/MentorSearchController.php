<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MentorSearchController extends Controller
{
    public function search($string){
        /**
         * 1 - administrator
         * 2 - moderator
         * 3 - mentor
         * 4 - student
         */
        $role_number = 3;
        $me = auth()->id();
        $mentors = \DB::select("SELECT
            u.id, u.name, u.student_no, u.photo_lg, u.email, u.discord, u.facebook_live, u.twitch, u.mixer,
            u.skill_summary, u.bio, ru.role_id
            FROM users as u LEFT JOIN role_user as ru ON u.id = ru.user_id
            WHERE ru.role_id = ? AND u.id != ? AND (u.name LIKE ? OR u.skill_summary LIKE ?)
        ",[$role_number,$me,$string.'%','%'.$string.'%']);
        return response()->json($mentors);
    }
}
