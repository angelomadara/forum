<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'reply' => $this->body,
            'user' => $this->user->name,
            'student_no' => $this->user->student_no,
            'created_at' => $this->created_at->diffForHumans(),
            // 'tag' => $this->category->name,
            'user_img' => $this->user->photo_lg,
        ];
    }
}
