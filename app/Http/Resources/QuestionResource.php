<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'title' => $this->title,
            'id' => $this->id,
            'user_img' => $this->user->photo_lg,
            'slug' => $this->slug,
            'path' => $this->path,
            'body' => $this->body,
            'created_at' => $this->created_at ? $this->created_at->diffForHumans() : null,
            'user_name' => $this->user->name,
            'user_id' => $this->user->id,
            'user_code' => $this->user->student_no,
            'replies' => ReplyResource::collection($this->replies),
            'replies_count' => $this->replies->count(),
            'tag' => $this->category->name,
        ];
    }
}
