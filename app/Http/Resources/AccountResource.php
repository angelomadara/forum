<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return $this;
        return [
            'name' => $this->name,
            'student_no' => $this->student_no,
            'email' => $this->email,
            'isActive' => $this->active,
            'email_confirmed' => $this->confirmed,
            'date_registered' => $this->created_at
        ];
    }
}
