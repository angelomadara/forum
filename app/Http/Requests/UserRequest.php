<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_no'    => "unique:users|required",
            'email'         => "unique:users|required|email|max:255",
            'name'          => "required",
            'password'      => "required|min:6|regex:'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$'",
            'compaired_password' => "required|min:6|same:password",
            'role'          => "required",
            'email_token'   => "nullable",
        ];
    }

    public function messages(){
        return [
            'compaired_password.same' => "Password didn't match",
            'password.regex' => "The password format is invalid. Password must be alpha-numeric and contains at least 1 capital letter",
            'password.min' => "The password must be at least 6 characters long.",
        ];
    }
}
