<?php

namespace App;

use App\Models\Conversation;
use App\Models\FollowMentor;
use App\Models\Question;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'student_no', 'email', 'password','active','confirmed','email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function question(){
        return $this->hasMany(Question::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * mutator
     */
    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }

    public function mentors(){
        return $this->hasMany(FollowMentor::class)->where('mentor_id',$this->id);
    }

    public function students(){
        return $this->hasMany(FollowMentor::class)->where('student_id',$this->id);
    }

    public function messages(){
        return $this->hasMany(Conversation::class)->where('to',$this->id);
    }
}
