<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationUsers extends Model
{
    protected $table = 'conversation_users';
    protected $primaryKey = 'id';
    protected $fillable = ['participants'];
    protected $hidden = ['updated_at', 'created_at'];
    
    public function messages(){
        return $this->hasMany(Conversation::class)->where('conversation_user_id',$this->id);
    }
}
