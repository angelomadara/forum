<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    /**
     * over write the show method on the controller
     * and use the `slug` field insted of `id`
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * mutate slug request
     */
    public function setSlugAttribute($value){
        $this->attributes['slug'] = str_slug($value);
    }
}
