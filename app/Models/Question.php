<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    // protected $fillable = ['title','slug','body','category_id','user_id'];
    protected $guarded = [];

    protected $with = ['replies'];

    protected static function boot(){
        parent::boot();
        static::creating(function($question){
            $question->slug = str_slug($question->title);
        });
    }

    /**
     * overwrite url and will not use the id of a record on the database and will use the slug
     * this will return a query with a slug provided in the url
     */
    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function replies(){
        return $this->hasMany(Reply::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * return the the `slug` as a `path` in the api
     */
    public function getPathAttribute(){
        // return 'question/'.$this->slug;
        return 'question/'.$this->id.'/'.$this->slug;
    }
}
