<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table = 'conversations';
    protected $guarded = [];

    public function participants(){
        return $this->belongsTo(ConversationUsers::class)->where('id',$this->conversation_user_id);
    }
}
