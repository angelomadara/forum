<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class FollowMentor extends Model
{
    use SoftDeletes;
    // protected $guarded = [];
    protected $table = 'follow_mentor';
    protected $primaryKey = 'id';
    protected $fillable = ['student_id','mentor_id'];
    protected $hidden = ['updated_at', 'deleted_at'];

    public function students(){
        return $this->belontsTo(User::class)->where('id',$this->student_id);
    }

    public function mentors(){
        return $this->belongsTo(User::class)->where('id',$this->mentor_id);
    }
}
