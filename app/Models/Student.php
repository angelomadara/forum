<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use FollowMentor;

class Student extends Model
{
    public function followedMe(){
        $me = auth()->id();
        // $mentor = new FollowMentor();
        // $mentor->where('mentor_id',$me)->students;
        $students = \DB::select("SELECT 
            u.*
        FROM follow_mentor AS fm LEFT JOIN users AS u ON fm.student_id = u.id
        WHERE fm.mentor_id = ? AND fm.deleted_at IS NULL",[$me]);
        return $students;
    }
}
