<?php


/**
 * API routes to access this route
 * type in the command `php artisan route:list`
 * this will show all the available routes
 *
 * note that this routes are protected by JWT (json web token)
 * see for the documentation of JSON web token `tymon/jwt-auth`
 * for reference
 */

Route::group(['prefix' => 'auth'], function () {
    Route::post('signup','AuthController@signup'); // signup route
    Route::post('login', 'AuthController@login'); // login route
    Route::post('logout', 'AuthController@logout'); // logout
    Route::post('refresh', 'AuthController@refresh'); // refresh token route
    Route::post('me', 'AuthController@me'); // show the logged in user route
    Route::post('payload', 'AuthController@payload'); // show user payload

    // Route::group(['middleware'=>['role:administrator|moderator|mentor|student']],function(){
        /**
         * this are the routes that the accessed by the accounts with role of:
         * administrator | moderator | mentor | student
         * apiResource = POST, GET, PUT or PATCH, DELETE
         */

        /**
         * Email confirmation
         */
        Route::get('/email/confirmation/{code}','AuthController@emailCheckCode');


        /**
         * profile and role
         */
        Route::get('/profile/{student_no}','AuthController@someone');
        Route::get('/role','AuthController@role');

        /**
         * account
         */
        Route::apiResource('/account','AccountsController');

        /**
         * mentor
         */
        Route::get('/mentor/following','MentorController@followed');
        Route::post('/mentor/unfollow','MentorController@unfollow');
        Route::get('/mentor/search/{string}','MentorSearchController@search');
        Route::apiResource('/mentor','MentorController');

        /**
         * student
         */
        Route::get('/student/all','StudentController@myStudents');

        /**
         * profile picture
         */
        Route::post('/profile/upload/avatar','ProfileController@changeAvatar');
        Route::apiResource('/profile','ProfileController');

        /**
         * questions
         */
        Route::get('/question/{id}/{slug}','QuestionController@showByIdAndSlug');
        Route::apiResource('/question','QuestionController');

        /**
         * categories or tags
         */
        Route::apiResource('/category','CategoryController');

        /**
         * reply
         */
        Route::post('post/reply','ReplyController@postReply');
        Route::apiResource('/question/{id}/{question}/reply','ReplyController');

        /**
         * like
         */
        Route::post('/like/{reply}','LikeController@like');
        Route::delete('/like/{reply}','LikeController@removeLike');

        /**
         * chat
         */
        Route::get('/chat/get/{id}','ConversationController@index');
        Route::get('/chat/{id}','ConversationController@chat');
        Route::post('/chat/send','ConversationController@send');

        /**
         * settings
         */
        Route::get('/settings/profile','SettingsController@profile');
    // });
});

// use App\Models\Role;
// use App\User;

// Route::get('role',function(){
    // $owner = new Role();
    // $owner->name = 'student';
    // $owner->display_name = 'Student';
    // $owner->description = 'Student';
    // $owner->save();
    // $user = User::where('id',1)->first();
    // $user->roles()->attach(1);
// });
