<?php
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

/**
 * redirect route
 */
Route::view('/','home');
// Route::get('confirm/email/{email_token}','AuthController@confirm_email');
/**
 * this will correct some unknown pages (page not found)
 * that generated by vue router
 */
Route::view('/{any}','home');
Route::view('/{any}/{another_any}','home');
Route::view('/{any}/{another_one}/{and_another_one}','home');

// Route::view('404','home');


Route::get('admin/config',function(){
    // return config('app.url');
    // return $_SERVER;
    // return encrypt('00-0001');
    // return abort(404);
    // $admin = User::find(1);
    // return $admin->roles()->attach(1);
});

