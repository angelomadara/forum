-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 01, 2019 at 12:11 AM
-- Server version: 5.7.28-0ubuntu0.19.04.2
-- PHP Version: 7.2.24-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forum_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'machine-learning', 'Machine Learning', '2019-10-02 17:43:32', '2019-10-02 17:43:32', NULL),
(7, 'laravel-basics', 'Laravel Basics', '2019-10-06 23:43:01', '2019-10-12 17:08:48', NULL),
(13, 'php', 'PHP', '2019-10-22 23:44:21', '2019-10-22 23:44:21', NULL),
(14, 'linux', 'Linux', NULL, NULL, NULL),
(15, 'csharp', 'c#', NULL, NULL, NULL),
(16, 'cplusplus', 'c++', NULL, NULL, NULL),
(17, 'phyton', 'phyton', NULL, NULL, NULL),
(18, 'npm', 'npm', NULL, NULL, NULL),
(19, 'node-js', 'node js', NULL, NULL, NULL),
(20, 'css', 'css', NULL, NULL, NULL),
(21, 'c', 'c', NULL, NULL, NULL),
(22, 'visual-basic', 'visual basic', NULL, NULL, NULL),
(23, 'bash', 'bash', NULL, NULL, NULL),
(24, 'cmd', 'cmd', NULL, NULL, NULL),
(25, 'fortran', 'fortran', NULL, NULL, NULL),
(26, 'cobol', 'cobol', NULL, NULL, NULL),
(27, 'lisp', 'Lisp', NULL, NULL, NULL),
(28, 'brainfuck', 'Brainfuck', NULL, NULL, NULL),
(29, 'java', 'Java', NULL, NULL, NULL),
(30, 'javascripit', 'Javascript', NULL, NULL, NULL),
(31, 'jquery', 'JQuery', NULL, NULL, NULL),
(32, 'angular-js', 'angular js', NULL, NULL, NULL),
(33, 'vue-js', 'Vue Js', NULL, NULL, NULL),
(34, 'coffee-script', 'Coffee Script', NULL, NULL, NULL),
(35, 'lua', 'Lua', NULL, NULL, NULL),
(36, 'mongo-db', 'Mongo DB', NULL, NULL, NULL),
(37, 'mysql', 'Mysql', NULL, NULL, NULL),
(38, 'objective-c', 'Objective C', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `follow_mentor`
--

CREATE TABLE `follow_mentor` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL COMMENT 'users id',
  `mentor_id` int(10) UNSIGNED NOT NULL COMMENT 'users id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `follow_mentor`
--

INSERT INTO `follow_mentor` (`id`, `student_id`, `mentor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 5, '2019-11-30 05:28:27', '2019-11-30 05:28:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `reply_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_01_034557_create_questions_table', 1),
(4, '2019_10_01_034628_create_replies_table', 1),
(5, '2019_10_01_034651_create_categories_table', 1),
(6, '2019_10_01_034659_create_likes_table', 1),
(7, '2019_10_03_092109_create_foreign_keys', 1),
(8, '2019_10_13_132524_entrust_setup_tables', 1),
(9, '2019_10_17_153842_add_student_number_onusers', 1),
(10, '2019_11_09_141954_email_options_in_users_table', 2),
(11, '2019_11_24_172705_new_settings_for_users', 3),
(12, '2019_11_28_175823_streaming_profiles', 4),
(13, '2019_11_30_122749_skill_summary_users_table', 5),
(14, '2019_11_30_130807_mentor', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `slug`, `title`, `body`, `category_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'how-to-boil-a-perfect-egg', 'how to boil a perfect egg?', 'How do i boil an egg?', 7, 3, '2019-11-14 11:56:33', '2019-11-14 11:56:33', NULL),
(2, 'sdf', 'sdf', 'sfdsfs', 13, 3, '2019-11-14 12:10:57', '2019-11-14 12:10:57', NULL),
(3, 'sadf', 'sadf', 'asdfsaf', 3, 3, '2019-11-14 12:12:00', '2019-11-14 12:12:00', NULL),
(4, 'windows-7-support-is-ending-a-guide-to-trying-linux', 'Windows 7 support is ending, a guide to trying Linux', 'After support ends (1/14/2020), Windows 7 is no longer going to receive security updates. Your computer will continue to work but since Windows 7 will be: 1) not uncommon, 2) lack security updates, 3) used mostly by people who are not tech/security savvy and who ignored the warnings... it will become the juiciest target for malware out there right now. It\'ll be open season on grandparents and small businesses who are left on it.\n\nSo you have 2 options:\n\n* upgrade to a newer version of Windows\n* switch to Linux\n* \nThis post is not going to be a Linux sales pitch. If you want Windows 10 and can run Windows 10 then install Windows 10.\n\n\"Well what is Linux?\"\nLinux is a free operating system with an emphasis on open source software. It is free to download, try out and install. It is however, not Windows. Some things will work differently and there will be some of a learning curve for a new user.\n\nSome things that work differently:\n\n* because it is free and open source, anyone can make their own version and distribute it to the community. We call each version put out by a particular group a \"distro\" for \"distribution.\" That\'s why you might see some ridiculously complicated graphs and suggestions for all the different versions of Linux (unlike with Windows where there are a few official versions all made by Microsoft)\n* because a lot of the software is free and open source, each distro can provide its users with a repository of a bunch of pre-packaged software designed to work on your system which you can install, run and update free of charge, this can then all be managed by a package manager. On the user side, this works like a phone app store where everything is free.\n* because it is free and open source, it can be seen as a threat to Microsoft making money so some software (Microsoft Office) will likely never be provided for Linux directly.\n* because it is less common than Windows, some companies simply don\'t make versions of their software for Linux. Adobe is a big one.\n\n\"Ok, how do I try it out?\"\ntry out some Linux software on Windows before switching\nCheckout https://alternativeto.net/ , and search for any software you tend to use. You can then try out a lot of the stuff available for Linux on Windows too. (edit: this includes MS Office alternatives).\n\nFor an organized list of linux software there’s this: https://wiki.archlinux.org/index.php/list_of_applications\n\nUse a virtual machine\nUsing software like virtual box https://www.virtualbox.org/ you can run Linux inside a program on Windows. This is a really great way to try it because you can do whatever you want in the virtual machine (VM) without changing things on your actual machine. The downside is that your computer has to run both Linux and Windows at the same time while doing this so it might not work well on low spec hardware and it won\'t let you test that all everything works on your hardware.\n\nBoot into the live environment\nLinux can actually run off of a USB stick. Doing this let\'s you test it out on your actual hardware and make sure that everything works correctly without changing anything on your harddrive. The downside is that you generally can\'t save changes while running off a USB stick so this is intended for testing, installing and recovering but not for extended use or experimentation.\n\n\"I\'m interested in trying it out, what should I start with?\"\nDo you want something that looks different from windows?\nUbuntu is the classic newbie friendly Linux distro. By default it has a desktop environment that looks a little more like Mac than Windows but is distinctly different from both. Most Linux instructions online will include help on Ubuntu and there\'s lots of good forums to help. Ubuntu releases a new version every 6 months but maintains a long term support (LTS) release that is supported for 5 years. The 6 month version will have newer software, the LTS version will have (increasingly) older software but still get security updates for that supported time.\n\nUbuntu\'s install guide is here: https://help.ubuntu.com/community/GraphicalInstall\n\nDo you want a windows like desktop environment?\nLinux Mint is an even more newbie friendly Linux distro that is based on Ubuntu\'s LTS release. It\'s essentially just LTS Ubuntu with some extra help like backup integration and a more windows-like UI. That means basically all of the Ubuntu guides and help carry over but you\'ll feel more at home coming from Windows 7.\n\nLinux Mint\'s install guide is here: https://linuxmint-installation-guide.readthedocs.io/en/latest/ (and it\'s very well put together for a newbie)\n\n\"Looks good! Now what?\"\nKnow that while you can do almost everything on Ubuntu and Mint through a graphical interface, a lot of advice and help will give you things to run on the commandline (even if you could do it other ways). Don\'t be scared of the commandline but also try to look up what you are running to understand it yourself; it\'s a way of telling your computer exactly what you want it to do... efficient and powerful if you know the basics but it\'s not a good idea to trust every random person who gives you a command to run from the internet.\n\nLinux malware/viruses are extremely rare and you won\'t get anything bad if you download from the official repositories. Part of the reason is that most of the time, you are not giving programs permission to install, update or change things on your system. To give them permission, you need to enter the \"root password\" so that anything where you aren\'t entering your password or typing \"sudo\" can only affect your documents and personal stuff but anything where you entering your password can make bigger changes to your system. Take note of anytime someone is telling you to add a new repository or download and run something from somewhere else.\n\nIf for some reason Ubuntu and Mint are not working for you, you can start branching out and trying other distros. Everyone\'s got their own favorite but these are just the best places to start. To learn in a more in-depth structured way check out: https://linuxjourney.com/', 14, 2, '2019-11-14 19:59:20', '2019-11-14 19:59:20', NULL),
(5, 'coca-cola', 'coca cola', '> Masarap ba ang coke?', 7, 5, '2019-11-23 23:05:54', '2019-11-24 00:04:28', '2019-11-24 00:04:28'),
(6, 'sdf', 'sdf', 'adsf', 7, 5, '2019-11-24 00:07:06', '2019-11-24 00:07:06', '2019-11-23 16:00:00'),
(7, 'aaaaaaaa', 'aaaaaaaa', 'asdfsadfdafsad', 7, 5, '2019-11-24 00:07:29', '2019-11-24 00:07:32', '2019-11-24 00:07:32'),
(8, 'what-is-array', 'what is array', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 7, 5, '2019-11-24 00:19:49', '2019-11-24 00:19:49', NULL),
(9, 'sadf', 'sadf', 'asdfsafsdafdsaf sadf sdafsad df asf', 13, 5, '2019-11-24 10:55:51', '2019-11-24 10:55:51', NULL),
(10, 'how-to-boil-a-perfect-egg', 'how to boil a perfect egg?', 'how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?how to boil a perfect egg?', 3, 5, '2019-11-24 11:06:37', '2019-11-24 11:06:37', NULL),
(11, 'sdf', 'sdf', 'asdffdsfsdfdfsdffsdfsdf', 7, 5, '2019-11-24 16:35:50', '2019-11-24 16:35:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'Administrator', NULL, NULL, NULL),
(2, 'moderator', 'Moderator', NULL, NULL, NULL),
(3, 'mentor', 'Mentor', NULL, NULL, NULL),
(4, 'student', 'Student', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(5, 3),
(2, 4),
(3, 4),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_lg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_sm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `discord` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_live` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mixer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mood` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `skill_summary` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT '0',
  `email_token` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `student_no`, `email`, `photo_lg`, `photo_sm`, `password`, `active`, `discord`, `facebook_live`, `twitch`, `mixer`, `mood`, `skype`, `linkedin`, `twitter`, `facebook`, `website`, `organization`, `bio`, `skill_summary`, `remember_token`, `confirmed`, `email_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '00-0000', 'admin@bpsuforum.com', NULL, NULL, '$2y$10$wrmH9BeZkf.e8OgyrcBmh.Eb/AXMWI0pgXCOnAmW9TvrLsyApw1ZW', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'op5NdBXVtF', 1, NULL, NULL, NULL),
(2, 'dafadsf', '19-0014', 'user@user.com', NULL, NULL, '$2y$10$CF.3GOxaqqIhl5Ow7xaJCOUgAkl/BdAH6cc.6zIhNsIJk5h5kPhlm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'eyJpdiI6IlFrQzAyNmdTWXZGdlhIYmE4TFlSU1E9PSIsInZhbHVlIjoiV3AydFpkWUFZNUZ5TTNSOGpwb2xuQT09IiwibWFjIjoiMTE5NDQ2YmU3OTAzYzczMTBjYzI3OWU1N2I3Y2E1OWJlMGNkZjdkMjgyNmFkODIxM2FkNjU3ZTg1NWJlZmMyZSJ9', '2019-11-09 06:59:32', '2019-11-09 06:59:32'),
(3, 'dafadsf', '19-0015', 'mxadara.olegna@hotmail.com', NULL, NULL, '$2y$10$RdrPPuckVbCGFRsiVBHhEOylcCTTWMa1NqJhlwlgWEqVD6lcf76lu', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'eyJpdiI6IlJDUFdVRDI0VWhIeFFHU2lyQ1MzckE9PSIsInZhbHVlIjoiVnRocUlJN3EwY05ZZFJKNjc3TUJQZz09IiwibWFjIjoiMTQ2YjQyYmM1YWI2MjZlOGY2ZDNiNDkwZDJiYWIzZmUxMGY1OGU2MDc5YzVjZjU2NGEwNjk4NzAxMTcwMDg4YSJ9', '2019-11-09 07:00:48', '2019-11-09 07:00:48'),
(4, 'dafadsf', '19-0018', 'madara.olegna@gmail.com', '/img/avatar/6lQoh1hJtItyK6A6.png', '/img/avatar/small/6lQoh1hJtItyK6A6.png', '$2y$10$6vFhCf7N5/236LfHp.oRA.dHZV/88yMKJQCC2KY/FrX2QPXUinWgS', 1, NULL, NULL, NULL, NULL, NULL, 'ads', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'eyJpdiI6ImV2UDluNnBpTnBWazBvTmlWVUZLRVE9PSIsInZhbHVlIjoiYnBNb3V4RXpoTDZucEtoQmRGSU9JQT09IiwibWFjIjoiOGFlZDRjOTE5MTQxZjIzZGZkOTk2ZmE1NjQzZTBhYTU0ZTg3Zjc4YTczNDRlYzNlOWRhMzVlZmZkOTViZjVkNSJ9', '2019-11-09 08:11:22', '2019-11-30 12:14:32'),
(5, 'Estanislao', '19-0011', 'madara.olegna@hotmail.com', '/img/avatar/sailormoon.jpeg', NULL, '$2y$10$wrmH9BeZkf.e8OgyrcBmh.Eb/AXMWI0pgXCOnAmW9TvrLsyApw1ZW', 1, 'lsjdflkjdsfkl', 'lksjdflkjsdf', 'sddsdsfsdfdsf', 'safsfd', 'sdfdsfdsfsdfdsfsdfsfdsfddd', 'Fisch', 'lkjadslk', 'lksjdflkjsf', NULL, 'lskjdflksfdsf', 'sdlfkjsdfsf', 'this is my bio', NULL, NULL, 0, 'eyJpdiI6Ikl6d2crclRJYVhYTDMyMU5rWVBqb1E9PSIsInZhbHVlIjoiOUpjTk1ZeWtEcStrZWZaMG95aGNXdz09IiwibWFjIjoiM2QwZmQ4Y2Y0ZGE3NmQ2MTcxYmE0ZGViZjdlOTg3YjhkZmJhZWUzNzk3NzI2ZDA0NjVkNDM1MzZjNWQ0MmM1MyJ9', '2019-11-14 21:25:23', '2019-11-28 11:08:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow_mentor`
--
ALTER TABLE `follow_mentor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `follow_mentor_student_id_foreign` (`student_id`),
  ADD KEY `follow_mentor_mentor_id_foreign` (`mentor_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_category_id_foreign` (`category_id`),
  ADD KEY `questions_user_id_foreign` (`user_id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `replies_question_id_foreign` (`question_id`),
  ADD KEY `replies_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_student_no_unique` (`student_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `follow_mentor`
--
ALTER TABLE `follow_mentor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `follow_mentor`
--
ALTER TABLE `follow_mentor`
  ADD CONSTRAINT `follow_mentor_mentor_id_foreign` FOREIGN KEY (`mentor_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `follow_mentor_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `questions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `replies`
--
ALTER TABLE `replies`
  ADD CONSTRAINT `replies_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `replies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
